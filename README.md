# Current ILDA Technical Committee Members


### Alain Neuens Director/Owner of LNS S.A. 

Alain finished his studies at the Institut Supérieur de Technologie in 
Luxembourg as an engineer  in electronics in 1992 where he build his own laser 
projection system. He was infected by the laser-virus at the early age of 16 in 
1985 with the support of AJSL, a contest for young scientists. In 1993 he 
organized his first large scale show for the Worlds93 hot air balloon world 
championship with an audience of some 30.000 spectators. Over the years he was 
involved in most of the major out and indoor shows in Luxembourg while his main 
business developed as an IT-company. He joined ILDA in 1998 and is a member 
since that date.


### Dirk Apitz

Freelance hardware and software developer, president of DexLogic and Beamwalk. 
Dirk started lasing in 1995 when he developed his own realtime animation system. 
He soon found, that the future of projector communication is networking and 
developed the ScanMaster series of projector control boards. The advanced 
rendering engines of these systems allowed for the direct input of curves and 
adjustments in fractions of microseconds instead of handling points. ScanMaster2 
received the Fenning Award for Technical Achievement in 2001. Recently he 
developed the FPGA-based StageMate/StageFeed converters to replace analog ISP 
cables with standard ethernet connections and works on the ILDA Digital Network 
(IDN) protocol. 


### Horacio Pugliese


### James Stewart 

James Stewart, BSc (Hons) MSc CMIOSH CLSO, is a UK based safety practitioner 
possessing occupational safety and health skills, along with specialist 
technical knowledge and understanding of display applications employing laser 
technology. In addition to his formal safety education, James has an academic 
background in physics, mathematics, electronic engineering, and computer 
science. He currently works as an independent consultant to many of the UK's 
major entertainment venues. His expertise is used by venue operators to ensure 
that the risks associated with laser display installations are sufficiently 
controlled and managed, protecting the safety of workers, performers and members 
of the public. James is the project lead for the IEC/TR 60825-3 safety guidance 
for laser display applications.


### Josh Salz


### Markus Steblei

Safety officer, technician and laser show producer. Markus started using a 
unique planetarium show laser system in Munich in 1996 and did his first 
programming attempts in 1998. Over the years and until 2005, he created over 50 
laser show sequences, including full shows like the famous "Queen - Heaven" 
Lasershow in 2001, followed by "Genesis - Tonight" in 2003/2004. These up to 
74-minute-long shows featuring both graphics and beam sequences have been 
created on LOBO LACON 3plus and LACON 5 systems with up to 6 laser scanners, 2 
on robots. He was also responsible for technical maintenance of those laser 
systems.


### Matthias Frank

Lecturer and senior researcher at the Insitute of Computer Science 4 at the 
University of Bonn, Germany. Matthias got a Diploma degree in Computer Science 
in 1994 from the University of Paderborn and a Ph.D. degree in Computer Science 
from the University of Bonn in 1999. His main research areas are covering 
communication systems, networking protocols and wireless/mobile communication. 
In 2004, Matthias started student activities in undergraduate courses in 
practical computer science with programming prototype software for control of 
laser projection systems and DMX-based light systems. The student's Laser & 
Light Lab (LLL) was founded. In 2009 Matthias (resp. the Institute of Computer Science 
4) joined ILDA as a non-profit member. Since 2013, Matthias and the Uni Bonn 
LLL are involved in the standardization activities of IDN (ILDA Digital Network), 
e.g. contributing with proof-of-concept implementations to several IDN use 
cases, like software driver for IDN producers (e.g. HE Laserscan, Medialas 
Mamba/M III, mylaserpage Mamba X4 and others), IDN-Toolbox to visualize 
received IDN streams, IDN plug-in for Wireshark, IDN-Switcher and others.


### Patrick Murphy

ILDA Executive Director since 2006. Member of ANSI and SAE committees working on 
laser safety. Co-founder with William R. Benner Jr. of Pangolin Laser Software 
(now Systems), helped design and program Laser Show Designer 2000 until the year 
2000.


### Paul Mehner


### Richard Gonsalves


### Sterling Bond


### Theo Dari

Computer engineer since 1993 and visual artist since 1998. Laser programmer since 2002. Creator and owner of french company Lasermen since 2005.
Inventor and first performer of World famous Laserman visual acts (2001) and Laserfighters act (2008). 
Created LAARIS interactive games system (2014) using Qt (C++17, Qml, Widgets...on Windows, Mac, Linux and embeddded systems).
Experienced in industrial vision systems since 2013. Long-time UX/UI design interest.

### Tim Hallmark

Staff Field Service Engineer at Coherent. Tim began working on lasers in 1987 at 
the university of Washington.  After graduating with a degree in Electrical 
Engineering, Tim began working at Laser Fantasy in 1990.  After ten years of 
doing installations as well as large shows (Power of Houston, Hong Kong 
Handover) Tim began working as a field service engineer for Coherent in 2000. 
While working at Coherent, Tim still stays involved in Laser Shows and ILDA, 
where he has been a member since 1990.


### Tim Walsh

Tim Walsh is a laserist in Central Texas. Through his company Laser Spectacles, 
Inc., he creates a wide variety of laser shows and installations. Tim was a 
founding member of ILDA, and served as ILDA President from 2007-2011, as well as 
numerous years as Awards Committee Chairman. Laser Spectacles has received over 
22 ILDA Awards over the years.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0OTgyMTc3LDUwMDM0Mzg4MV19
-->